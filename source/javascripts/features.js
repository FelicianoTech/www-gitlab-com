(function () {
  var $videoButton = $(".video-button");
  var $videoContainer = $('.video-container');

  $videoButton.on('click', function (e) {
    e.preventDefault();
    $videoButton.hide();
    $videoContainer.show();
  });
})();
