---
layout: markdown_page
title: JIRA GitLab integration
---

Please see the [GitLab JIRA integration documentation](https://docs.gitlab.com/ee/user/project/integrations/jira.html#gitlab-jira-integration).