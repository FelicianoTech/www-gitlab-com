## Senior Developers

Senior Developers are experienced developers who meet the following requirements:

1. Technical Skills
    * Write modular, well-tested, and maintainable code
    * Know a domain really well and radiate that knowledge
    * Contribute to one or more complementary projects
    * Take on difficult issues, big or small
2. Leadership
    * Begin to show architectural perspective
    * Propose new ideas, performing feasibility analyses and scoping the work
3. Code quality
    * Leave code in substantially better shape than before
    * Fix bugs/regressions quickly
    * Monitor overall code quality/build failures
    * Leave tests in better shape than before: faster, more reliable, more
      comprehensive, etc.
4. Communication
    * Provide thorough and timely code feedback for peers
    * Able to communicate clearly on technical topics and present ideas to the
      rest of the team
    * Keep issues up-to-date with progress
    * Help guide other merge requests to completion
5. Performance and scalability
    * Excellent at writing production-ready code with little assistance
    * Are able to write complex code that can scale with a significant number of users
    * Are able to fix performance issues on GitLab.com with minimal guidance using
      our existing tools, and improve those tools where needed
